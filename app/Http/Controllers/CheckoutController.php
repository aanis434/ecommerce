<?php

namespace App\Http\Controllers;

use Stripe\Stripe;
use Stripe\Charge;
use Cart;
use Session;
use Mail;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{


    public function index()
    {
        if(Cart::content()->count()==0)
        {
            Session::flash('info','Your Cart is still Empty. Do Some shopping.');

            return redirect()->back();
        }
        return view('checkout');
    }

    public function pay()
    {
        
        Stripe::setApikey("sk_test_BGfRyH02aNwV8rX94kAfHPQX00H92ErZjp");

        $charge = Charge::create([
            'amount' => Cart::total() * 100, 
            'currency' => 'bdt', 
            'description' => 'E-commerce Book Shop', 
            'source' => request()->stripeToken
            ]);

            Session::flash('success','Purchase Successfull.Please Wait for our Mail.');

            Mail::to(request()->stripeEmail)->send(new \App\Mail\PurchaseSuccessfull);

            Cart::destroy();
            return redirect('/');
    }


}
