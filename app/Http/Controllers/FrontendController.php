<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;


class FrontendController extends Controller
{
    public function index()
    {
        return view('index')->with('products',Product::paginate(6));
    }

    public function single($id)
    {
        return view('single')->with('product',Product::find($id));
    }
  
}
