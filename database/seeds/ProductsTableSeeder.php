<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $p1 = [
        //     'name' => 'Learn to build websites in HTML5',
        //     'image' => 'uploads/products/book5.png',
        //     'price' => '500',
        //     'description' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Natus blanditiis qui, earum minus nihil molestiae quos sed tenetur temporibus sequi pariatur architecto facilis similique voluptate repellendus perferendis iusto sint autem.'
        // ];

        // $p2 = [
        //     'name' => 'Learn to build websites in CSS3',
        //     'image' => 'uploads/products/book5.png',
        //     'price' => '600',
        //     'description' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Natus blanditiis qui, earum minus nihil molestiae quos sed tenetur temporibus sequi pariatur architecto facilis similique voluptate repellendus perferendis iusto sint autem.'
        // ];

        // App\Product::create($p1);
        // App\Product::create($p2);

        factory(\App\Product::class,30)->create();
    }
}
