@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $title }}</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped table-responsive">
                        <tr>
                            <th>#SL</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Price</th>
                            <th>Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        @foreach($products as $key=>$product)
                        <tr>
                           
                            <td>{{ $key+1 }}</td>
                            <td>{{ $product->name }}</td>
                            <td> <img src="{{ $product->image }}" width="50px" height="50px" alt=""> </td>
                            <td> {{ $product->price }} </td>
                            <td> {!! $product->description !!} </td>
                        <td> <a href="/product/{{ $product->id }}/edit" class="btn btn-sm btn-info">Edit</a> </td>
                            <td>
                            <form action="{{ route('product.destroy',['id'=>$product->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn btn-sm btn-danger">Delete</button>
                                </form>
                            </td>
                           
                        </tr>
                        @endforeach
                    </table>
                    
                </div>
                <div class="panel-footer text-center">
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
