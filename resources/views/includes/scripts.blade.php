<script src="{{ asset('front/js/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('front/js/crum-mega-menu.js') }}"></script>
<script src="{{ asset('front/js/swiper.jquery.min.js') }}"></script>
<script src="{{ asset('front/js/theme-plugins.js') }}"></script>
<script src="{{ asset('front/js/main.js') }}"></script>
<script src="{{ asset('front/js/form-actions.js') }}"></script>

<script src="{{ asset('front/js/velocity.min.js') }}"></script>
<script src="{{ asset('front/js/ScrollMagic.min.js') }}"></script>
<script src="{{ asset('front/js/animation.velocity.min.js') }}"></script>

<script src="{{ asset('front/js/toastr.min.js') }}"></script>
    <script>
        @if(Session::has('success'))
            toastr.success('{{ Session::get('success') }}');
        @endif

        @if(Session::has('info'))
            toastr.info('{{ Session::get('info') }}');
        @endif
    </script>