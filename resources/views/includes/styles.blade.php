<link rel="stylesheet" type="text/css" href="{{ asset('front/css/fonts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/crumina-fonts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/normalize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/grid.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/styles.css') }}">


    <!--Plugins styles-->

    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/jquery.mCustomScrollbar.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/swiper.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/primary-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/magnific-popup.css') }}">

    
    <link href="{{ asset('front/css/toastr.min.css') }}" rel="stylesheet">

    <!--Styles for RTL-->

    <!--<link rel="stylesheet" type="text/css" href="{{ asset('front/css/rtl.css') }}">-->

    <!--External fonts-->

    <link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>